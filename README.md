# Towards a Robust Detection of Language Model-Generated Text: <br /> Is ChatGPT that easy to detect?

The repos contains the data used in our paper, in addition to a finetunning script to train a language model detector.

## Summary:
Recent advances in natural language processing (NLP) have led to the development of large language
models (LLMs) such as ChatGPT. Our paper proposes a methodology for developing and evaluating
ChatGPT detectors for French text, with a focus on investigating their robustness on out-of-domain
data and against common attack schemes. The proposed method involves translating an English
dataset into French and training a classifier on the translated data. Results show that the detectors
can effectively detect ChatGPT-generated text, with a degree of robustness against basic attack
techniques in in-domain settings. However, vulnerabilities are evident in out-of-domain contexts,
highlighting the challenge of detecting adversarial text. The study emphasizes caution when applying
in-domain testing results to a wider variety of content. We provide our translated datasets, as well as our adversarial and out-domain datasets. Our  models are made availavle as open-source resources in this repo.


### Data Set Overview:

This dataset is made of two parts:

- First, an extension of the Human ChatGPT Comparison Corpus (HC3) dataset with French data automatically translated from the English source.
- Second, out-of-domain and adversarial French data set have been gathereed (Human adversarial, BingGPT, Native French ChatGPT responses).



### Main Results Overview:

Long Story Short: Detection models are highly efficient to detect in-domain data but much less so when facing out-of domain and adversarial data. Of course, they are vulnerable to additional noise.

![Results](./imgs/table1.png)

![Results](./imgs/table2.png)

## Datasets

For a detailed dataset description, please refer to the [dataset card](https://huggingface.co/datasets/almanach/hc3_french_ood).

The multilingual dataset used to train XLM-R is a concatenation of the French and English subsets.

To generate homoglyph substitution and misspelling attacks, we used the [nlpaug](https://github.com/makcedward/nlpaug) library as follows:

```python
import nlpaug.augmenter.char as nac

with open("data/stopwords-fr.txt", "r") as f:
    stopwords_fr = f.read().splitlines()

with open("data/stopwords-en.txt", "r") as f:
    stopwords_en = f.read().splitlines()

ocr_aug = nac.OcrAug(
    aug_char_p=0.3,
    aug_char_min=1,
    aug_char_max=1,
    aug_word_p=0.1,
    stopwords=stopwords_fr,
)

key_aug_en = nac.KeyboardAug(
    aug_char_p=0.3,
    aug_char_min=1,
    aug_char_max=1,
    aug_word_p=0.05,
    stopwords=stopwords_en,
)
key_aug_fr = nac.KeyboardAug(
    aug_char_p=0.3,
    aug_char_min=1,
    aug_char_max=1,
    aug_word_p=0.05,
    stopwords=stopwords_fr,
    lang="fr",
)
```

## Models

We provide the following fine-tuned models:

| Model | Dataset |
| --- | --- |
| [CamemBERTa](https://huggingface.co/almanach/camemberta-chatgptdetect-noisy) | `hc3_fr_full +ms +hg` |
| [XLM-R](https://huggingface.co/almanach/xlmr-chatgptdetect-noisy) | `hc3_multilingual_full +ms +hg` |


## Training

You can use the following command to train a detector on the subset of your choice.
The script can directly evaluate on `qa`, `sentence` or `full` subsets.

```bash
RESULTS_DIR=
LOGGING_DIR=
SUBTASK=
LANG=
GRADIENT_ACCUMULATION_STEPS=
LEARNING_RATE=
EPOCHS=
LR_SCHEDULER=
WARMUP_STEPS=
SEED=

python run_cls.py \
--model_name_or_path $RESULTS_DIR \
--task_name $SUBTASK \
--dataset_name almanach/hc3_multi \
--output_dir $RESULTS_DIR/evals \
--logging_dir $LOGGING_DIR \
--logging_strategy steps \
--logging_steps 100 \
--log_level debug \
--save_strategy epoch \
--do_eval \
--do_predict \
--test_is_validation true \
--evaluation_subsets \"sentence,full,qa\" \
--evaluation_strategy epoch \
--load_best_model_at_end \
--metric_for_best_model f1 \
--greater_is_better true \
--per_device_train_batch_size 8 \
--per_device_eval_batch_size 8 \
--gradient_accumulation_steps $GRADIENT_ACCUMULATION_STEPS \
--eval_accumulation_steps 4 \
--fp16 \
--learning_rate $LEARNING_RATE \
--num_train_epochs $EPOCHS \
--lr_scheduler_type $LR_SCHEDULER \
--warmup_ratio $WARMUP_STEPS \
--seed $SEED
```

## Citation

If you use our dataset or models, please cite our paper:

```bibtex
@proceedings{towards-a-robust-2023-antoun,
    title = "Towards a Robust Detection of Language Model-Generated Text: Is ChatGPT that easy to detect?",
    editor = "Antoun, Wissam  and
      Mouilleron, Virginie  and
      Sagot, Benoit  and
      Seddah, Djam{\'e}",
    month = "6",
    year = "2023",
    address = "Paris, France",
    publisher = "ATALA",
    url = "https://gitlab.inria.fr/wantoun/robust-chatgpt-detection/-/raw/main/towards_chatgpt_detection.pdf",
}
```


```bibtex
@article{guo-etal-2023-hc3,
    title = "How Close is ChatGPT to Human Experts? Comparison Corpus, Evaluation, and Detection",
    author = "Guo, Biyang  and
      Zhang, Xin  and
      Wang, Ziyuan  and
      Jiang, Minqi  and
      Nie, Jinran  and
      Ding, Yuxuan  and
      Yue, Jianwei  and
      Wu, Yupeng",
    journal={arXiv preprint arxiv:2301.07597}
    year = "2023",
    url = "https://arxiv.org/abs/2301.07597"
}
```
