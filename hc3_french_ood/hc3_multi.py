"""HC3 French + ChatGPT/BingGPT QA Pairs + FAQ QA Pairs"""

import json
import os

import datasets

# Find for instance the citation on arxiv or on the dataset repo/website
_CITATION = """\
# TODO: Add BibTeX citation for our TALN 2023 paper:
Towards a Robust Detection of Language Model-Generated Text: Is ChatGPT that easy to detect?

@article{guo-etal-2023-hc3,
    title = "How Close is ChatGPT to Human Experts? Comparison Corpus, Evaluation, and Detection",
    author = "Guo, Biyang  and
      Zhang, Xin  and
      Wang, Ziyuan  and
      Jiang, Minqi  and
      Nie, Jinran  and
      Ding, Yuxuan  and
      Yue, Jianwei  and
      Wu, Yupeng",
    journal={arXiv preprint arxiv:2301.07597}
    year = "2023",
}
"""

# You can copy an official description
_DESCRIPTION = """\
Human ChatGPT Comparison Corpus (HC3) Translated To French.
The translation is done by Google Translate API.
We also add the native french QA pairs from ChatGPT, BingGPT and FAQ pages.

This dataset was used in our TALN 2023 paper.
Towards a Robust Detection of Language Model-Generated Text: Is ChatGPT that easy to detect?
"""

_HOMEPAGE = "https://huggingface.co/datasets/almanach/hc3_french_ood"


_LICENSE = "Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)"

_DATA_PATH = "data"

_URLs = {
    "hc3_en": _DATA_PATH + "/hc3_en",
    "hc3_fr": _DATA_PATH + "/hc3_fr",
    "hc3_fr_chatgpt": _DATA_PATH + "/hc3_fr_chatgpt",
    "faq_fr_gouv": _DATA_PATH,
    "faq_fr_random": _DATA_PATH,
    "qa_fr_binggpt": _DATA_PATH,
    "qa_fr_binglikehuman": _DATA_PATH,
}

_PATH_MAP = {
    "hc3_fr_qa": _URLs["hc3_fr"] + "/{split}_qa_examples.json",
    "hc3_fr_sentence": _URLs["hc3_fr"] + "/{split}_sentence_examples.json",
    "hc3_fr_full": _URLs["hc3_fr"] + "/{split}_full_examples.json",
    "hc3_en_qa": _URLs["hc3_en"] + "/{split}_qa_examples.json",
    "hc3_en_sentence": _URLs["hc3_en"] + "/{split}_sentence_examples.json",
    "hc3_en_full": _URLs["hc3_en"] + "/{split}_full_examples.json",
    "hc3_fr_chatgpt_qa": _URLs["hc3_fr_chatgpt"] + "/test_qa_examples_chatgpt.jsonl",
    "qa_fr_binggpt": _URLs["qa_fr_binggpt"] + "/qa_binggpt.jsonl",
    "qa_fr_binglikehuman": _URLs["qa_fr_binglikehuman"] + "/qa_binglikehuman.jsonl",
    "faq_fr_gouv": _URLs["faq_fr_gouv"] + "/faq_fr_gouv.jsonl",
    "faq_fr_random": _URLs["faq_fr_random"] + "/faq_fr_random.jsonl",
}

logger = datasets.logging.get_logger(__name__)


class Hc3Fr(datasets.GeneratorBasedBuilder):
    """Human ChatGPT Comparison Corpus (HC3) Translated To French"""

    VERSION = datasets.Version("1.0.0")

    BUILDER_CONFIGS = [
        datasets.BuilderConfig(
            name="hc3_fr_qa",
            version=VERSION,
            description="Human ChatGPT Comparison Corpus (HC3) Translated To French - QA Pairs",
        ),
        datasets.BuilderConfig(
            name="hc3_fr_sentence",
            version=VERSION,
            description="Human ChatGPT Comparison Corpus (HC3) Translated To French - Sentence level",
        ),
        datasets.BuilderConfig(
            name="hc3_fr_full",
            version=VERSION,
            description="Human ChatGPT Comparison Corpus (HC3) Translated To French - Full conversation",
        ),
        datasets.BuilderConfig(
            name="hc3_en_qa",
            version=VERSION,
            description="Human ChatGPT Comparison Corpus (HC3) English - QA Pairs",
        ),
        datasets.BuilderConfig(
            name="hc3_en_sentence",
            version=VERSION,
            description="Human ChatGPT Comparison Corpus (HC3) English - Sentence level",
        ),
        datasets.BuilderConfig(
            name="hc3_en_full",
            version=VERSION,
            description="Human ChatGPT Comparison Corpus (HC3) English - Full conversation",
        ),
        datasets.BuilderConfig(
            name="hc3_fr_chatgpt_qa",
            version=VERSION,
            description="Sampled Question from Human ChatGPT Comparison Corpus (HC3) Translated To French then answered by ChatGPT",
        ),
        datasets.BuilderConfig(
            name="qa_fr_binggpt",
            version=VERSION,
            description="QA Pairs from BingGPT",
        ),
        datasets.BuilderConfig(
            name="qa_fr_binglikehuman",
            version=VERSION,
            description="QA Pairs with human answers mimicking BingGPT style",
        ),
        datasets.BuilderConfig(
            name="faq_fr_gouv",
            version=VERSION,
            description="FAQ Pairs from french government websites filtered from MQA dataset (subset 'fr-faq-page'). https://huggingface.co/datasets/clips/mqa",
        ),
        datasets.BuilderConfig(
            name="faq_fr_random",
            version=VERSION,
            description="FAQ Pairs from random rench websites filtered from MQA dataset (subset 'fr-faq-page'). https://huggingface.co/datasets/clips/mqa",
        ),
    ]

    def _info(self):
        if self.config.name in ["hc3_fr_qa", "hc3_en_qa"]:
            features = datasets.Features(
                {
                    "id": datasets.Value("string"),
                    "question": datasets.Value("string"),
                    "answer": datasets.Value("string"),
                    "label": datasets.features.ClassLabel(names=["HUMAN", "CHATGPT"]),
                    "source": datasets.Value("string"),
                }
            )

        elif self.config.name in [
            "hc3_fr_sentence",
            "hc3_en_sentence",
            "hc3_fr_full",
            "hc3_en_full",
        ]:
            features = datasets.Features(
                {
                    "id": datasets.Value("string"),
                    "text": datasets.Value("string"),
                    "label": datasets.features.ClassLabel(names=["HUMAN", "CHATGPT"]),
                    "source": datasets.Value("string"),
                }
            )
        elif self.config.name == "hc3_fr_chatgpt_qa":
            features = datasets.Features(
                {
                    "id": datasets.Value("string"),
                    "question": datasets.Value("string"),
                    "answer": datasets.Value("string"),
                    "chatgpt_answer": datasets.Value("string"),
                    "label": datasets.features.ClassLabel(names=["HUMAN", "CHATGPT"]),
                    "source": datasets.Value("string"),
                }
            )
        elif self.config.name == "qa_fr_binggpt":
            features = datasets.Features(
                {
                    "id": datasets.Value("string"),
                    "question": datasets.Value("string"),
                    "answer": datasets.Value("string"),
                    "label": datasets.features.ClassLabel(names=["HUMAN", "BINGGPT"]),
                    "deleted_clues": datasets.Value("string"),
                    "deleted_sources": datasets.Value("string"),
                    "remarks": datasets.Value("string"),
                }
            )
        elif self.config.name == "qa_fr_binglikehuman":
            features = datasets.Features(
                {
                    "id": datasets.Value("string"),
                    "question": datasets.Value("string"),
                    "answer": datasets.Value("string"),
                    "label": datasets.features.ClassLabel(names=["HUMAN", "BINGGPT"]),
                    "source": datasets.Value("string"),
                }
            )
        elif self.config.name == "faq_fr_gouv":
            features = datasets.Features(
                {
                    "id": datasets.Value("string"),
                    "page_id": datasets.Value("string"),
                    "question_id": datasets.Value("string"),
                    "answer_id": datasets.Value("string"),
                    "bucket": datasets.Value("string"),
                    "domain": datasets.Value("string"),
                    "question": datasets.Value("string"),
                    "answer": datasets.Value("string"),
                    "label": datasets.features.ClassLabel(names=["HUMAN", "CHATGPT"]),
                }
            )
        elif self.config.name == "faq_fr_random":
            features = datasets.Features(
                {
                    "id": datasets.Value("string"),
                    "page_id": datasets.Value("string"),
                    "question_id": datasets.Value("string"),
                    "answer_id": datasets.Value("string"),
                    "bucket": datasets.Value("string"),
                    "domain": datasets.Value("string"),
                    "question": datasets.Value("string"),
                    "answer": datasets.Value("string"),
                    "label": datasets.features.ClassLabel(names=["HUMAN", "CHATGPT"]),
                }
            )
        else:
            raise ValueError(
                "Invalid config name. Must be one of the following: "
                + ", ".join(self.BUILDER_CONFIGS)
            )

        return datasets.DatasetInfo(
            # This is the description that will appear on the datasets page.
            description=_DESCRIPTION,
            # This defines the different columns of the dataset and their types
            features=features,  # Here we define them above because they are different between the two configurations
            # If there's a common (input, target) tuple from the features,
            # specify them here. They'll be used if as_supervised=True in
            # builder.as_dataset.
            # supervised_keys=supervised_keys,
            # Homepage of the dataset for documentation
            homepage=_HOMEPAGE,
            # License for the dataset if available
            license=_LICENSE,
            # Citation for the dataset
            citation=_CITATION,
        )

    def _split_generators(self, dl_manager):
        """Returns SplitGenerators."""

        extracted_path = dl_manager.download_and_extract("data.tar.gz")
        # assert extracted_path != " "
        path = os.path.join(extracted_path, _PATH_MAP[self.config.name])
        # path = extracted_path + _PATH_MAP[self.config.name]
        if self.config.name in [
            "hc3_fr_qa",
            "hc3_en_qa",
            "hc3_fr_sentence",
            "hc3_en_sentence",
            "hc3_fr_full",
            "hc3_en_full",
        ]:
            return [
                datasets.SplitGenerator(
                    name=datasets.Split.TRAIN,
                    gen_kwargs={
                        "file_path": path.format(split="train"),
                        "split": "train",
                    },
                ),
                datasets.SplitGenerator(
                    name=datasets.Split.VALIDATION,
                    gen_kwargs={
                        "file_path": path.format(split="val"),
                        "split": "val",
                    },
                ),
                datasets.SplitGenerator(
                    name=datasets.Split.TEST,
                    gen_kwargs={
                        "file_path": path.format(split="test"),
                        "split": "test",
                    },
                ),
            ]
        else:
            return [
                datasets.SplitGenerator(
                    name=datasets.Split.TEST,
                    gen_kwargs={
                        "file_path": path,
                        "split": "test",
                    },
                ),
            ]

    def _generate_examples(self, file_path, split):
        """Yields examples."""
        # Yields (key, example) tuples from the dataset

        with open(file_path, encoding="utf-8") as f:
            data = [json.loads(line) for line in f]
            for id_, row in enumerate(data):
                if self.config.name in ["hc3_fr_qa", "hc3_en_qa"]:
                    yield id_, {
                        "id": str(row["id"]) + "_" + str(row["entry_id"]),
                        "question": row["question"],
                        "answer": row["answer"],
                        "label": "HUMAN" if row["is_human"] == 1 else "CHATGPT",
                        "source": row["source"],
                    }
                elif self.config.name in [
                    "hc3_fr_sentence",
                    "hc3_en_sentence",
                ]:
                    yield id_, {
                        "id": str(row["id"])
                        + "_"
                        + str(row["entry_id"])
                        + "_"
                        + str(row["sentence_id"]),
                        "text": row["text"],
                        "label": "HUMAN" if row["is_human"] == 1 else "CHATGPT",
                        "source": row["source"],
                    }
                elif self.config.name in [
                    "hc3_fr_full",
                    "hc3_en_full",
                ]:
                    yield id_, {
                        "id": str(row["id"]) + "_" + str(row["entry_id"]),
                        "text": row["text"],
                        "label": "HUMAN" if row["is_human"] == 1 else "CHATGPT",
                        "source": row["source"],
                    }
                elif self.config.name == "hc3_fr_chatgpt_qa":
                    if "chatgpt_answer" not in row:
                        continue
                    yield id_, {
                        "id": str(row["id"]) + "_" + str(row["entry_id"]),
                        "question": row["question"],
                        "answer": row["answer"],
                        "chatgpt_answer": row["chatgpt_answer"],
                        "label": "CHATGPT",
                        "source": row["source"],
                    }
                elif self.config.name == "qa_fr_binggpt":
                    yield id_, {
                        "id": str(row["id"]),
                        "question": row["question"],
                        "answer": row["answer"],
                        "label": "BINGGPT",
                        "deleted_clues": row["deleted_clues"],
                        "deleted_sources": row["deleted_sources"],
                        "remarks": row["remarks"],
                    }
                elif self.config.name == "qa_fr_binglikehuman":
                    yield id_, {
                        "id": str(row["id"]),
                        "question": row["question"],
                        "answer": row["answer"],
                        "label": "HUMAN",
                        "source": row["source"],
                    }
                elif self.config.name in ["faq_fr_gouv", "faq_fr_random"]:
                    yield id_, {
                        "id": str(id_),
                        "page_id": row["page_id"],
                        "question_id": row["question_id"],
                        "answer_id": row["answer_id"],
                        "bucket": row["bucket"],
                        "domain": row["domain"],
                        "question": row["question"],
                        "answer": row["answer"],
                        "label": "HUMAN",
                    }
                else:
                    raise ValueError("Invalid config name")
