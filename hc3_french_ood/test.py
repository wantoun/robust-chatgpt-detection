# %%
import datasets

# %%
hc3_fr_qa = datasets.load_dataset(
    "almanach/hc3_french_ood",
    "hc3_fr_qa",
)
print(hc3_fr_qa)
hc3_fr_sentence = datasets.load_dataset(
    "almanach/hc3_french_ood",
    "hc3_fr_sentence",
)
print(hc3_fr_sentence)
hc3_fr_full = datasets.load_dataset(
    "almanach/hc3_french_ood",
    "hc3_fr_full",
)
print(hc3_fr_full)
hc3_en_qa = datasets.load_dataset(
    "almanach/hc3_french_ood",
    "hc3_en_qa",
)
print(hc3_en_qa)
hc3_en_sentence = datasets.load_dataset(
    "almanach/hc3_french_ood",
    "hc3_en_sentence",
)
print(hc3_en_sentence)
hc3_en_full = datasets.load_dataset(
    "almanach/hc3_french_ood",
    "hc3_en_full",
)
print(hc3_en_full)
hc3_fr_chatgpt_qa = datasets.load_dataset(
    "almanach/hc3_french_ood",
    "hc3_fr_chatgpt_qa",
)
print(hc3_fr_chatgpt_qa)
qa_fr_binggpt = datasets.load_dataset(
    "almanach/hc3_french_ood",
    "qa_fr_binggpt",
)
print(qa_fr_binggpt)
qa_fr_binglikehuman = datasets.load_dataset(
    "almanach/hc3_french_ood",
    "qa_fr_binglikehuman",
)
print(qa_fr_binglikehuman)
faq_fr_gouv = datasets.load_dataset(
    "almanach/hc3_french_ood",
    "faq_fr_gouv",
)
print(faq_fr_gouv)
faq_fr_random = datasets.load_dataset(
    "almanach/hc3_french_ood",
    "faq_fr_random",
)
print(faq_fr_random)

# %%
